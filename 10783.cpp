#include <cstdio>
#include <algorithm>
using namespace std;

int main()
{
	int a, b, t, i, sum=0;
	scanf("%d", &t);
	for(int j=1;j<=t;++j){
        scanf("%d%d", &a, &b);

        if(a<b){
            for(i=a;i<=b;++i){
                if(i%2==1)
                    sum+=i;
            }
        }
        else{
            for(i=b;i<=a;++i){
                if(i%2==1)
                    sum+=i;
            }
        }
        printf("Case %d: %d\n", j, sum);
        sum=0;
	}

	return 0;
}
